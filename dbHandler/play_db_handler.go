package dbHandler

import (
	"database/sql"
	"sync"
	"log"
	"time"
	play "playyDbServer"
	"strconv"
	"github.com/kellydunn/golang-geo"
	"strings"
	"errors"
)

// Will be abstraction over multiple databases in the backend.
type PlayDb struct {
	_rdb *sql.DB
	query_map_lock *sync.Mutex
	_prepared_queries map[string]*sql.Stmt
}

// Error codes.
const SUCCESS = 0
const UUID_GEN_ERROR = 100
const SQL_INSERT_ERROR = 101
const SQL_PREPARE_ERROR = 102
const SQL_OTHER_ERROR = 103
const SQL_NO_ROWS = 104
const SQL_QUERY_ERROR = 105
const NUM_CONV_ERROR = 106
const INCORRECT_REQ = 107

// Function to open connection. Ideally be called only once.
// TODO: Make this generic to connect to all types of DBs.
func connect_sql_db() *sql.DB {

	rdb, err := sql.Open("mysql",
		"root:root@tcp(127.0.0.1:3306)/mugsSchema")

	if err != nil {
		log.Fatal(time.Now().String() + "Failed sql.Open. Fatal error %v.", err)
		return nil
	}

	err = rdb.Ping()

	if err != nil {
		log.Println(time.Now().String() + "Ping failed after sql.Open.")
		return nil
	}

	log.Println(time.Now().String() + "Opened new connection to DB")

	return rdb
}

//TODO: Make this generic for all types of DB.
func close_sql_db(db *sql.DB) {
	db.Close()
	log.Println(time.Now().String() + "Closed connection to DB")
}

func NewPlayDb() *PlayDb {

	db := new(PlayDb)
	db._rdb = connect_sql_db()
	db._prepared_queries = make(map[string]*sql.Stmt, 20)
	db.query_map_lock = new(sync.Mutex)

	return db
}

func (db *PlayDb) GetPreparedStatement(query_string string) (*sql.Stmt, error) {

	// If new connection was opened, old prepared statements
	// are no longer valid.
	var stmt *sql.Stmt
	var err error

	db.query_map_lock.Lock()

	if db.RefreshRDB() {
		for i := range db._prepared_queries {
			delete(db._prepared_queries, i)
		}
	}

	stmt = db._prepared_queries[query_string]

	if stmt == nil {
		stmt, err = db._rdb.Prepare(query_string)
	} else {
		delete(db._prepared_queries, query_string)
	}
	db.query_map_lock.Unlock()

	return stmt, err
}

func (db *PlayDb) ReleaseStatement(stmt *sql.Stmt, query_string string) {

	db.query_map_lock.Lock()
	// If a duplicate statement exists just close it.
	if stmt_exists := db._prepared_queries[query_string]; stmt_exists != nil {
		stmt.Close()
	} else {
		if len(db._prepared_queries) < 20 {
			db._prepared_queries[query_string] = stmt
		} else {
			stmt.Close()
		}
	}
	db.query_map_lock.Unlock()
}

func (db *PlayDb) RefreshRDB() bool {

	// If not reachable, open new connection.
	if err := db._rdb.Ping(); err != nil {
		close_sql_db(db._rdb)
		db._rdb = connect_sql_db()
		return true
	}
	return false
}

func (db *PlayDb) GetAppError(status int32, arg interface{}) error {
	var errStr string

	switch status {
	case SUCCESS:
		return nil
	case NUM_CONV_ERROR:
	case INCORRECT_REQ:
		errStr = "Incorrect Request"
		log.Println("Error String:%s Func:%s Arg:%+v", errStr, MyCaller(), arg)
		break
	case UUID_GEN_ERROR:
		errStr = "UUID gen error"
		log.Println("Error String:%s Func:%s Arg:%+v", errStr, MyCaller(), arg)
		break
	case SQL_INSERT_ERROR:
		errStr = "SQL insert error"
		log.Println("Error String:%s Func:%s Arg:%+v", errStr, MyCaller(), arg)
		break
	case SQL_PREPARE_ERROR:
		errStr = "SQL prepare error"
		log.Println("Error String:%s Func:%s Arg:%+v", errStr, MyCaller(), arg)
		break
	case SQL_OTHER_ERROR:
		errStr = "SQL other error"
		log.Println("Error String:%s Func:%s Arg:%+v", errStr, MyCaller(), arg)
		break
	case SQL_QUERY_ERROR:
		errStr = "SQL query error"
		log.Println("Error String:%s Func:%s Arg:%+v", errStr, MyCaller(), arg)
		break
	//Error will be supressed if not an error in some cases.
	case SQL_NO_ROWS:
		errStr = "SQL no rows"
		log.Println("Error String:%s Func:%s Arg:%+v", errStr, MyCaller(), arg)
		break
	default:
		panic("Unknown error code!")
	}
	return errors.New(errStr)
}

// All the fields should be checked before coming here.
func (db *PlayDb) CreateUserDB(user_req *play.UserInfo) (*play.UserInfo, int32) {

	if found, _, obj := db.checkDuplicateInsert(*user_req); found == true {
		return obj.(*play.UserInfo), SUCCESS
	}

	var ret int32
	var stmt *sql.Stmt
	var err error
	var db_query_str string
	var res sql.Result

	if uuidStr, e := NewUUID(); e != nil {
		ret = UUID_GEN_ERROR
		goto out
	} else {
		user_req.UserUuid = uuidStr
	}

	db_query_str = "INSERT INTO Users(user_uuid, name, email, mobile, gcm_reg_id) VALUES(?, ?, ?, ?, ?)"

	stmt, err = db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	res, err = stmt.Exec(user_req.UserUuid,
		user_req.Name,
		user_req.Email,
		user_req.Mobile,
		user_req.GcmRegId)

	if err != nil {
		ret = SQL_INSERT_ERROR
		goto out
	}

	if ck, e := res.RowsAffected(); e != nil && ck == 1 {
		log.Println("Successfully inserted user:%+v", user_req)
		ret = SUCCESS
		go db.CreateNotification(WELCOME_MSG, user_req)
		goto out
	} else {
		ret = SQL_OTHER_ERROR
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, user_req)
	}
	return user_req, ret
}

func (db * PlayDb) GetUserByIdDB(user_uuid *play.ReqUUID) (*play.UserInfo, int32)  {

	var ret_user play.UserInfo
	var ret int32
	var row *sql.Row

	db_query_str := "SELECT * FROM Users WHERE user_uuid = ?"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	row = stmt.QueryRow(user_uuid.Uuid)

	if e := row.Scan(&ret_user.UserUuid,
		&ret_user.Name,
		&ret_user.Email,
		&ret_user.Mobile,
		&ret_user.GcmRegId); e != nil {

		ret = SQL_QUERY_ERROR
	} else {
		ret = SUCCESS
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, user_uuid)
	}
	return &ret_user, ret
}

func (db * PlayDb) GetUserByInfoDB(user_info *play.UserInfo) (*play.UserInfo, int32)  {

	var ret_user play.UserInfo
	var ret int32
	var db_query_str string
	var arg string
	var row *sql.Row

	// Based on email or mobile.
	if len(user_info.Email) > 0 {
		db_query_str = "SELECT * FROM Users WHERE email = ?"
		arg = user_info.Email
	} else {
		db_query_str = "SELECT * FROM Users WHERE mobile = ?"
		arg = user_info.Mobile
	}

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	row = stmt.QueryRow(arg)

	if e := row.Scan(&ret_user.UserUuid,
		&ret_user.Name,
		&ret_user.Email,
		&ret_user.Mobile,
		&ret_user.GcmRegId); e != nil {

		ret = SQL_QUERY_ERROR
	} else {
		ret = SUCCESS
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, user_info)
	}
	return &ret_user, ret
}

// Venue related routines.
func (db *PlayDb) GetVenuesByLocationDB(loc *play.LocationInfo) (*play.Venues, int32) {

	var venue play.Venue
	var ret int32
	var r, lt, ln float64
	var err error
	var p *geo.Point
	var s *geo.SQLMapper
	var rows *sql.Rows

	venues := new(play.Venues)

	if lt, err = strconv.ParseFloat(loc.Latitude, 64); err != nil {
		ret = NUM_CONV_ERROR
		goto out
	} else if ln, err = strconv.ParseFloat(loc.Longitude, 64); err != nil {
		ret = NUM_CONV_ERROR
		goto out
	}

	if loc.Radius != "" {
		if r, err = strconv.ParseFloat(loc.Radius, 64); err != nil {
			ret = NUM_CONV_ERROR
			goto out
		}
	} else {
		r = 20.0
	}

	p = geo.NewPoint(lt, ln)

	s, err = geo.HandleWithSQL()
	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	rows, err = s.PointsWithinRadius(p, r)
	if err != nil {
		ret = SQL_QUERY_ERROR
		goto out
	}

	defer rows.Close()

	for rows.Next() {
		if e := rows.Scan(&venue.VenueUuid,
			&venue.VenueName,
			&venue.VenueUrl,
			&venue.DomainName,
			&venue.DomainId,
			&venue.Address_1,
			&venue.Address_2,
			&venue.City,
			&venue.State,
			&venue.Zip,
			&venue.Latitude,
			&venue.Longitude,
			&venue.EventCatVenue); e == nil {

			venues.Venues = append(venues.Venues, &venue)
		}
	}
	ret = SUCCESS
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, loc)
	}
	return venues, ret
}

func (db * PlayDb) GetVenueByIdDB(venue_uuid *play.ReqUUID) (*play.Venue, int32)  {

	var ret_venue play.Venue
	var ret int32
	var row *sql.Row

	db_query_str := "SELECT * FROM Venues WHERE venue_uuid = ?"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	row = stmt.QueryRow(venue_uuid.Uuid)

	if e := row.Scan(&ret_venue.VenueUuid,
		&ret_venue.VenueName,
		&ret_venue.VenueUrl,
		&ret_venue.DomainName,
		&ret_venue.DomainId,
		&ret_venue.Address_1,
		&ret_venue.Address_2,
		&ret_venue.City,
		&ret_venue.State,
		&ret_venue.Zip,
		&ret_venue.Latitude,
		&ret_venue.Longitude,
		&ret_venue.EventCatVenue); e != nil {

		ret = SQL_QUERY_ERROR
	} else {
		ret = SUCCESS
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, venue_uuid)
	}
	return &ret_venue, ret
}

// Venue has been pre-Vetted for nil values.
func (db * PlayDb) CreateVenueDB(req_venue *play.Venue) (*play.Venue, int32) {

	if found, _, obj := db.checkDuplicateInsert(*req_venue); found == true {
		return obj.(*play.Venue), SUCCESS
	}

	var ret int32
	var err error
	var stmt *sql.Stmt
	var db_query_str string
	var res sql.Result

	if uuidStr, e := NewUUID(); e != nil {
		ret = UUID_GEN_ERROR
		goto out
	} else {
		req_venue.VenueUuid = uuidStr
	}

	db_query_str = "INSERT INTO Venues VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

	stmt, err = db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	res, err = stmt.Exec(req_venue.VenueUuid,
		req_venue.VenueName,
		req_venue.VenueUrl,
		req_venue.DomainName,
		req_venue.DomainId,
		req_venue.Address_1,
		req_venue.Address_2,
		req_venue.City,
		req_venue.State,
		req_venue.Zip,
		req_venue.Latitude,
		req_venue.Longitude,
		req_venue.EventCatVenue)

	if err != nil {
		ret = SQL_INSERT_ERROR
		goto out
	}

	if ck, e := res.RowsAffected(); e != nil && ck == 1 {
		log.Println("Successfully inserted venue:%+v", req_venue)
		ret = SUCCESS
		goto out
	} else {
		ret = SQL_OTHER_ERROR
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, req_venue)
	}
	return req_venue, ret
}

// Event related routines.
func (db *PlayDb) GetEventsByVenueDB(req_uuid *play.ReqUUID) (*play.Events, int32) {

	var event play.Event
	var ret int32
	var rows *sql.Rows
	var err error
	var stmt *sql.Stmt

	events := new(play.Events)

	db_query_str := "SELECT * FROM Events WHERE venue_uuid = ?"

	stmt, err = db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	rows, err = stmt.Query(req_uuid.Uuid)

	if err != nil {
		ret = SQL_QUERY_ERROR
		goto out
	}

	defer rows.Close()

	for rows.Next() {
		// Skip if error in scanning.
		if e := rows.Scan(&event.EventUuid,
			&event.EventName,
			&event.EventUrl,
			&event.DomainName,
			&event.DomainId,
			&event.EventStart,
			&event.EventEnd,
			&event.VenueUuid,
			&event.OrganizerUuid,
			&event.Description,
			&event.EventCat,
			&event.EventType); e == nil {

			events.Events = append(events.Events, &event)
		}
	}
	ret = SUCCESS
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, req_uuid)
	}
	return events, ret
}

func (db *PlayDb) GetEventByIdDB(req_uuid *play.ReqUUID) (*play.Event, int32) {

	var event play.Event
	var ret int32
	var row *sql.Row

	db_query_str := "SELECT * FROM Events WHERE event_uuid = ?"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	row = stmt.QueryRow(req_uuid.Uuid)

	if e := row.Scan(&event.EventUuid,
		&event.EventName,
		&event.EventUrl,
		&event.DomainName,
		&event.DomainId,
		&event.EventStart,
		&event.EventEnd,
		&event.VenueUuid,
		&event.OrganizerUuid,
		&event.Description,
		&event.EventCat,
		&event.EventType); e != nil {

		ret = SQL_QUERY_ERROR
	} else {
		ret = SUCCESS
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, req_uuid)
	}
	return &event, ret
}

func (db *PlayDb) CreateEventDB(req_event *play.Event) (*play.Event, int32) {

	if found, _, obj := db.checkDuplicateInsert(*req_event); found == true {
		return obj.(*play.Event), 0
	}

	var ret int32
	var err error
	var stmt *sql.Stmt
	var db_query_str string
	var res sql.Result

	if uuidStr, e := NewUUID(); e != nil {
		ret = UUID_GEN_ERROR
		goto out
	} else {
		req_event.EventUuid = uuidStr
	}

	db_query_str = "INSERT INTO Events VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

	stmt, err = db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	res, err = stmt.Exec(req_event.EventUuid,
		req_event.EventName,
		req_event.EventUrl,
		req_event.DomainName,
		req_event.DomainId,
		req_event.EventStart,
		req_event.EventEnd,
		req_event.VenueUuid,
		req_event.OrganizerUuid,
		req_event.Description,
		req_event.EventCat,
		req_event.EventType)

	if err != nil {
		ret = SQL_INSERT_ERROR
		goto out
	}

	if ck, e := res.RowsAffected(); e != nil && ck == 1 {
		log.Println("Successfully inserted event:%+v", req_event)
		ret = SUCCESS
		goto out
	} else {
		ret = SQL_OTHER_ERROR
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, req_event)
	}
	return req_event, ret
}

// Organizer
func (db *PlayDb) CreateOrganizerDB(org *play.Organizer) (*play.Organizer, int32) {

	if found, _, obj := db.checkDuplicateInsert(*org); found == true {
		return obj.(*play.Organizer), SUCCESS
	}

	var ret int32
	var err error
	var stmt *sql.Stmt
	var db_query_str string
	var res sql.Result

	if uuidStr, e := NewUUID(); e != nil {
		ret = UUID_GEN_ERROR
		_ = db.GetAppError(ret, org)
		goto out
	} else {
		org.OrgUuid = uuidStr
	}

	db_query_str = "INSERT INTO Organizers VALUES(?, ?, ?, ?, ?, ?, ?)"

	stmt, err = db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	res, err = stmt.Exec(org.OrgUuid,
		org.OrgName,
		org.OrgUrl,
		org.OrgEmail,
		org.OrgPh,
		org.OrgContactName,
		org.UserUuid)

	if err != nil {
		ret = SQL_INSERT_ERROR
		goto out
	}

	if ck, e := res.RowsAffected(); e != nil && ck == 1 {
		log.Println("Successfully inserted organizer:%+v", org)
		ret = SUCCESS
	} else {
		ret = SQL_OTHER_ERROR
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, org)
	}
	return org, ret
}

func (db *PlayDb) GetOrganizerByIdDB(org_uuid *play.ReqUUID) (*play.Organizer, int32) {

	var ret int32
	var ret_org play.Organizer
	var row *sql.Row

	db_query_str := "SELECT * FROM Organizers WHERE org_uuid = ?"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	row = stmt.QueryRow(org_uuid.Uuid)

	if e := row.Scan(&ret_org.OrgUuid,
			&ret_org.OrgName,
			&ret_org.OrgUrl,
			&ret_org.OrgEmail,
			&ret_org.OrgPh,
			&ret_org.OrgContactName,
			&ret_org.UserUuid); e != nil {
		ret = SQL_NO_ROWS
	} else {
		ret = SUCCESS
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, org_uuid)
	}
	return &ret_org, ret
}

func (db *PlayDb) GetEventAvailabilityInfoDB(ev_uuid *play.ReqUUID) (*play.EventAvailabilityInfo, int32) {

	var ea_info play.EventAvailabilityInfo
	var ret int32
	var row *sql.Row

	db_query_str := "SELECT * FROM EventAvailabilityInfo WHERE event_uuid = ?"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	row = stmt.QueryRow(ev_uuid.Uuid)

	if e := row.Scan(&ea_info.EventUuid,
		&ea_info.Cost,
		&ea_info.SalesStart,
		&ea_info.SalesEnd,
		&ea_info.TotalAvailable,
		&ea_info.Sold,
		&ea_info.Available); e != nil {

		ret = SQL_QUERY_ERROR
	} else {
		ret = SUCCESS
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, ev_uuid)
	}
	return &ea_info, ret
}

func (db *PlayDb) GetEventPlayersInfoDB(ev_uuid *play.ReqUUID) (*play.EventPlayersInfo, int32) {

	var ep_info play.EventPlayersInfo
	var ret int32
	var row *sql.Row

	db_query_str := "SELECT * FROM EventPlayersInfo WHERE event_uuid = ?"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	row = stmt.QueryRow(ev_uuid.Uuid)

	if e := row.Scan(&ep_info.EventUuid,
		&ep_info.MinPlayers,
		&ep_info.MaxPlayers,
		&ep_info.MinAge,
		&ep_info.MaxAge); e != nil {

		ret = SQL_QUERY_ERROR
	} else {
		ret = SUCCESS
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, ev_uuid)
	}
	return &ep_info, ret
}

func (db *PlayDb) CreateEventJoinReqDB(join_req *play.EventJoinRequest) (*play.EventJoinRequest, int32) {

	if found, _, obj := db.checkDuplicateInsert(*join_req); found == true {
		return obj.(*play.EventJoinRequest), SUCCESS
	}

	var ret int32
	var res sql.Result

	join_req.CreatedAt = uint32(time.Now().Unix())

	db_query_str := "INSERT INTO EventJoinRequests(event_uuid, org_uuid, user_uuid, created_at) VALUES (?, ?, ?, ?)"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	res, err = stmt.Exec(join_req.EventUuid,
		join_req.OrgUuid,
		join_req.UserUuid,
		join_req.CreatedAt)

	if err != nil {
		ret = SQL_INSERT_ERROR
		goto out
	}

	if ck, e := res.RowsAffected(); e != nil && ck == 1 {
		log.Println("Successfully created join request:%+v", join_req)
		ret = SUCCESS
	} else {
		ret = SQL_OTHER_ERROR
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, join_req)
	}
	return join_req, ret
}

func (db *PlayDb) CreateEventInviteReqDB(invite *play.EventInvite) (*play.EventInvite, int32) {

	if found, _, obj := db.checkDuplicateInsert(*invite); found == true {
		return obj.(*play.EventInvite), SUCCESS
	}

	var ret int32
	var res sql.Result

	invite.CreatedAt = uint32(time.Now().Unix())

	db_query_str := "INSERT INTO EventInvites(event_uuid, user_uuid, invite_code, created_at) VALUES (?, ?, ?, ?)"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	res, err = stmt.Exec(invite.EventUuid,
		invite.UserUuid,
		invite.InviteCode,
		invite.CreatedAt)

	if err != nil {
		ret = SQL_INSERT_ERROR
		goto out
	}

	if ck, e := res.RowsAffected(); e != nil && ck == 1 {
		log.Println("Successfully created invite request:%+v", invite)
		ret = SUCCESS
	} else {
		ret = SQL_OTHER_ERROR
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, invite)
	}
	return invite, ret
}

func (db *PlayDb) CreateEventParticipantDB(player *play.EventParticipant) (*play.EventParticipant, int32) {

	if found, _, obj := db.checkDuplicateInsert(*player); found == true {
		return obj.(*play.EventParticipant), SUCCESS
	}

	var ret int32
	var res sql.Result

	player.JoinedAt = uint32(time.Now().Unix())

	db_query_str := "INSERT INTO EventParticipants(event_uuid, org_uuid, user_uuid, joined_at) VALUES (?, ?, ?, ?)"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	res, err = stmt.Exec(player.EventUuid,
		player.OrgUuid,
		player.UserUuid,
		player.JoinedAt)

	if err != nil {
		ret = SQL_INSERT_ERROR
		goto out
	}

	if ck, e := res.RowsAffected(); e != nil && ck == 1 {
		log.Println("Successfully created participation request:%+v", player)
		ret = SUCCESS
	} else {
		ret = SQL_OTHER_ERROR
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, player)
	}
	return player, ret
}

func (db *PlayDb) GetEventsParticipationDB(user_uuid *play.ReqUUID) (*play.EventParticipations, int32) {

	var ep_info play.EventParticipant
	var ret int32
	var rows *sql.Rows

	participations := play.EventParticipations{}

	db_query_str := "SELECT * FROM EventParticipants WHERE user_uuid = ?"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	rows, err = stmt.Query(user_uuid.Uuid)

	if err != nil {
		ret = SQL_QUERY_ERROR
		goto out
	}

	defer rows.Close()

	for rows.Next() {
		if e := rows.Scan(&ep_info.EventUuid,
			&ep_info.OrgUuid,
			&ep_info.UserUuid,
			&ep_info.JoinedAt); e == nil {

			participations.MyEvent = append(participations.MyEvent, &ep_info)
		}
	}
	ret = SUCCESS

out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, user_uuid)
	}
	return &participations, ret
}

func (db *PlayDb) AcceptEventInviteDB(invite *play.EventInvite) (*play.EventInvite, int32) {

	var ret int32
	var res sql.Result

	invite.AcceptedAt = uint32(time.Now().Unix())

	db_query_str := "UPDATE EventInvites SET accepted_at = ?, acc_user_uuid = ? " +
		"WHERE event_uuid = ? AND user_uuid = ? AND invite_code = ?"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	res, err = stmt.Exec(invite.AcceptedAt,
		invite.AccUserUuid,
		invite.EventUuid,
		invite.UserUuid,
		invite.InviteCode)

	if err != nil {
		ret = SQL_INSERT_ERROR
		goto out
	}

	if ck, e := res.RowsAffected(); e != nil && ck == 1 {
		log.Println("Successfully created invite request:%+v", invite)
		ret = SUCCESS
		go db.CreateNotification(INVITE_ACCEPTED, invite)
	} else {
		ret = SQL_OTHER_ERROR
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, invite)
	}
	return invite, ret
}

func (db *PlayDb) AcceptEventJoinReqDB(join_req *play.EventJoinRequest) (*play.EventJoinRequest, int32) {

	var ret int32
	var res sql.Result

	join_req.AcceptedAt = uint32(time.Now().Unix())

	db_query_str := "UPDATE EventJoinRequests SET accepted_at = ?" +
		"WHERE event_uuid = ? AND user_uuid = ?"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	res, err = stmt.Exec(join_req.AcceptedAt, join_req.EventUuid, join_req.UserUuid)

	if err != nil {
		ret = SQL_INSERT_ERROR
		goto out
	}

	if ck, e := res.RowsAffected(); e != nil && ck == 1 {
		log.Println("Successfully updated join request:%+v", join_req)
		ret = SUCCESS
		participant := new(play.EventParticipant)
		participant.UserUuid = join_req.UserUuid
		participant.EventUuid = join_req.EventUuid
		participant.OrgUuid = join_req.OrgUuid
		participant.JoinedAt = join_req.AcceptedAt
		_ , ret = db.CreateEventParticipantDB(participant)
		if ret == SUCCESS {
			go db.CreateNotification(REQUEST_ACCEPTED, join_req)
		}
	} else {
		ret = SQL_OTHER_ERROR
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, join_req)
	}
	return join_req, ret
}

func (db *PlayDb) UpdateSentEventInviteDB(invite *play.EventInvite) (*play.EventInvite, int32) {

	var ret int32
	var res sql.Result

	invite.SentAt = uint32(time.Now().Unix())

	db_query_str := "UPDATE EventInvites SET sent_at = ? WHERE event_uuid = ? AND user_uuid = ? AND invite_code = ?"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	res, err = stmt.Exec(invite.SentAt, invite.EventUuid, invite.UserUuid, invite.InviteCode)

	if err != nil {
		ret = SQL_INSERT_ERROR
		goto out
	}

	if ck, e := res.RowsAffected(); e != nil && ck == 1 {
		log.Println("Successfully created invite request:%+v", invite)
		ret = SUCCESS
	} else {
		ret = SQL_OTHER_ERROR
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, invite)
	}
	return invite, ret
}

func (db *PlayDb) UpdateSentEventJoinReqDB(join_req *play.EventJoinRequest) (*play.EventJoinRequest, int32) {

	var ret int32
	var res sql.Result

	join_req.SentAt = uint32(time.Now().Unix())

	db_query_str := "UPDATE EventJoinRequests SET sent_at = ? WHERE event_uuid = ? AND org_uuid = ? AND user_uuid = ?"

	stmt, err := db.GetPreparedStatement(db_query_str)

	if err != nil {
		ret = SQL_PREPARE_ERROR
		goto out
	}

	defer db.ReleaseStatement(stmt, db_query_str)

	res, err = stmt.Exec(join_req.SentAt, join_req.EventUuid, join_req.OrgUuid, join_req.UserUuid)

	if err != nil {
		ret = SQL_INSERT_ERROR
		goto out
	}

	if ck, e := res.RowsAffected(); e != nil && ck == 1 {
		log.Println("Successfully updated join request:%+v", join_req)
		ret = SUCCESS
	} else {
		ret = SQL_OTHER_ERROR
	}
out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, join_req)
	}
	return join_req, ret
}

func (db *PlayDb) checkDuplicateInsert(check interface{}) (bool, int32, interface{}) {

	var db_query_str string
	var stmt *sql.Stmt
	var err error
	var rows *sql.Rows
	var ret int32

	//Default
	ret = SUCCESS

	switch t := check.(type) {
	default:
		log.Println("USO!%v", t)
		goto err_out
	case play.Organizer:
		db_query_str = "SELECT * FROM Organizer WHERE org_email = ? OR org_ph = ?"
		stmt, err = db.GetPreparedStatement(db_query_str)
		if err != nil {
			ret = SQL_PREPARE_ERROR
			goto err_out
		}

		rows, err = stmt.Query(check.(play.Organizer).OrgEmail,
			check.(play.Organizer).OrgPh)
		
		if err != nil {
			ret = SQL_QUERY_ERROR
			goto err_out
		}
		defer rows.Close()
		dupOrg := new(play.Organizer)
		if rows.Next() {
			if e := rows.Scan(&dupOrg.OrgUuid,
				&dupOrg.OrgName,
				&dupOrg.OrgUrl,
				&dupOrg.OrgEmail,
				&dupOrg.OrgPh,
				&dupOrg.OrgContactName); e == nil {
				return true, SUCCESS, &dupOrg
			} else {
				ret = SQL_QUERY_ERROR
				goto err_out
			}
		}
		break
		
	case play.Event:
		db_query_str = "SELECT * FROM Events WHERE venue_uuid = ? AND org_uuid = ? AND event_start = ?"
		stmt, err = db.GetPreparedStatement(db_query_str)
		if err != nil {
			ret = SQL_PREPARE_ERROR
			goto err_out
		}
		rows, err = stmt.Query(check.(play.Event).VenueUuid,
			check.(play.Event).OrganizerUuid,
			check.(play.Event).EventStart)

		if err != nil {
			ret = SQL_QUERY_ERROR
			goto err_out
		}
		defer rows.Close()
		dupEvent := new(play.Event)
		if rows.Next() {
			if e := rows.Scan(&dupEvent.EventUuid,
				&dupEvent.EventName,
				&dupEvent.EventUrl,
				&dupEvent.DomainName,
				&dupEvent.DomainId,
				&dupEvent.EventStart,
				&dupEvent.EventEnd,
				&dupEvent.VenueUuid,
				&dupEvent.OrganizerUuid,
				&dupEvent.Description,
				&dupEvent.EventCat,
				&dupEvent.EventType); e == nil {
				return true, SUCCESS, &dupEvent
			} else {
				ret = SQL_QUERY_ERROR
				goto err_out
			}
		}
		break

	case play.Venue:
		// Simple check: Get venues 0.5 miles around.
		// If there are any results try to match based on names.
		loc := play.LocationInfo{Longitude:check.(play.Venue).Longitude,
			Latitude:check.(play.Venue).Latitude, Radius:"0.5"}

		nearby_venues, st := db.GetVenuesByLocationDB(&loc)

		if len(nearby_venues.Venues) > 0 && st == SUCCESS {
			for i := range nearby_venues.Venues {
				if strings.Contains(nearby_venues.Venues[i].VenueName, check.(play.Venue).VenueName) ||
					strings.Contains(check.(play.Venue).VenueName, nearby_venues.Venues[i].VenueName) {
					return true, SUCCESS, nearby_venues.Venues[i]
				}
			}
		}
		break

	case play.EventAvailabilityInfo:
		db_query_str = "SELECT * FROM EventAvailabilityInfo WHERE event_uuid = ?"
		stmt, err = db.GetPreparedStatement(db_query_str)
		if err != nil {
			ret = SQL_PREPARE_ERROR
			goto err_out
		}
		row := stmt.QueryRow(check.(play.EventAvailabilityInfo).EventUuid)
		dupEAInfo := new (play.EventAvailabilityInfo)
		if e := row.Scan(&dupEAInfo.EventUuid,
				&dupEAInfo.Cost,
				&dupEAInfo.SalesStart,
				&dupEAInfo.SalesEnd,
				&dupEAInfo.TotalAvailable,
				&dupEAInfo.Sold,
				&dupEAInfo.Available); e != nil {
			if e != sql.ErrNoRows {
				ret = SQL_QUERY_ERROR
			}
			goto err_out
		} else {
			return true, SUCCESS, &dupEAInfo
		}
		break
	case play.EventPlayersInfo:
		db_query_str = "SELECT * FROM EventPlayersInfo WHERE event_uuid = ?"
		stmt, err = db.GetPreparedStatement(db_query_str)
		if err != nil {
			ret = SQL_PREPARE_ERROR
			goto err_out
		}
		dupEPInfo := new (play.EventPlayersInfo)
		row := stmt.QueryRow(check.(play.EventPlayersInfo).EventUuid)
		if e := row.Scan(&dupEPInfo.EventUuid,
				&dupEPInfo.MinPlayers,
				&dupEPInfo.MaxPlayers,
				&dupEPInfo.MinAge,
				&dupEPInfo.MaxAge); e != nil {
			if e != sql.ErrNoRows {
				ret = SQL_QUERY_ERROR
			}
			goto err_out
		} else {
			return true, SUCCESS, &dupEPInfo
		}
		break
	case play.UserInfo:
		db_query_str = "SELECT * FROM Users WHERE email = ? OR mobile = ?"
		stmt, err = db.GetPreparedStatement(db_query_str)
		if err != nil {
			ret = SQL_PREPARE_ERROR
			goto err_out
		}
		rows, err = stmt.Query(check.(play.UserInfo).Email,
			check.(play.UserInfo).Mobile)

		if err != nil {
			ret = SQL_QUERY_ERROR
			goto err_out
		}
		defer rows.Close()
		dupUser := new(play.UserInfo)
		if rows.Next() {
			if e := rows.Scan(&dupUser.UserUuid,
					&dupUser.Name,
					&dupUser.Email,
					&dupUser.Mobile,
					&dupUser.GcmRegId); e == nil {
				return true, SUCCESS, &dupUser
			} else {
				ret = SQL_QUERY_ERROR
				goto err_out
			}
		}
		break
	case play.EventJoinRequest:
		db_query_str = "SELECT * FROM EventJoinRequests WHERE event_uuid = ? AND user_uuid = ?"
		stmt, err = db.GetPreparedStatement(db_query_str)
		if err != nil {
			ret = SQL_PREPARE_ERROR
			goto err_out
		}
		dupJoinReq := new (play.EventJoinRequest)
		row := stmt.QueryRow(check.(play.EventJoinRequest).EventUuid, check.(play.EventJoinRequest).UserUuid)
		if e := row.Scan(&dupJoinReq.EventUuid,
			&dupJoinReq.OrgUuid,
			&dupJoinReq.UserUuid,
			&dupJoinReq.CreatedAt,
			&dupJoinReq.SentAt,
			&dupJoinReq.AcceptedAt); e != nil {
			if e != sql.ErrNoRows {
				ret = SQL_QUERY_ERROR
			}
			goto err_out
		} else {
			return true, SUCCESS, &dupJoinReq
		}
		break
	case play.EventInvite:
		db_query_str = "SELECT * FROM EventInvites WHERE event_uuid = ? AND user_uuid = ? AND invite_code = ?"
		stmt, err = db.GetPreparedStatement(db_query_str)
		if err != nil {
			ret = SQL_PREPARE_ERROR
			goto err_out
		}
		dupInvite := new (play.EventInvite)
		row := stmt.QueryRow(check.(play.EventInvite).EventUuid, check.(play.EventInvite).UserUuid,
			check.(play.EventInvite).InviteCode)
		if e := row.Scan(&dupInvite.EventUuid,
			&dupInvite.UserUuid,
			&dupInvite.InviteCode,
			&dupInvite.CreatedAt,
			&dupInvite.SentAt,
			&dupInvite.AcceptedAt); e != nil {
			if e != sql.ErrNoRows {
				ret = SQL_QUERY_ERROR
			}
			goto err_out
		} else {
			return true, SUCCESS, &dupInvite
		}
		break
	case play.EventParticipant:
		db_query_str = "SELECT * FROM EventParticipants WHERE event_uuid = ? AND user_uuid = ?"
		stmt, err = db.GetPreparedStatement(db_query_str)
		if err != nil {
			ret = SQL_PREPARE_ERROR
			goto err_out
		}
		dupPartReq := new (play.EventParticipant)
		row := stmt.QueryRow(check.(play.EventJoinRequest).EventUuid, check.(play.EventJoinRequest).UserUuid)
		if e := row.Scan(&dupPartReq.EventUuid,
			&dupPartReq.OrgUuid,
			&dupPartReq.UserUuid,
			&dupPartReq.JoinedAt); e != nil {
			if e != sql.ErrNoRows {
				ret = SQL_QUERY_ERROR
			}
			goto err_out
		} else {
			return true, SUCCESS, &dupPartReq
		}
		break
	}
err_out:
	if ret != SUCCESS {
		_ = db.GetAppError(ret, db_query_str)
	}
	return false, ret, nil
}