package dbHandler

import (
	"github.com/alexjlockwood/gcm"
	"github.com/fatih/structs"
	"log"
	play "playyDbServer"
	"time"
	"strconv"
)

// Notifications type
const WELCOME_MSG = 0
const JOIN_REQUEST = 1
const REQUEST_ACCEPTED = 2
const INVITE_ACCEPTED = 3

// Event types
const PUBLIC = 0
const PRIVATE = 1
const CAMP = 2

func getTimeStampString(evTime time.Time) string {
	var timeStamp string
	timeStamp = "on " + evTime.Month().String() + ", "
	switch evTime.Day() {
	case 1:
		timeStamp += "1st"
		break
	case 2:
		timeStamp += "2nd"
		break
	case 3:
		timeStamp += "3rd"
		break
	case 21:
		timeStamp += "21st"
		break
	case 22:
		timeStamp += "22nd"
		break
	case 23:
		timeStamp += "23rd"
		break
	case 31:
		timeStamp += "31st"
		break
	default:
		timeStamp += strconv.Itoa(evTime.Day()) + "th"
	}
	return timeStamp
}

func (db *PlayDb) canInviteMorePeople(event *play.Event, details *play.EventAvailabilityInfo) (bool, int32) {

	if event.EventType == PUBLIC && details.TotalAvailable == 0 {
		return true, 0
	} else if event.EventType == PUBLIC && details.TotalAvailable > 0  && details.Available > 0 {
		return true, int32(details.Available)
	} else if event.EventType == PRIVATE && details.TotalAvailable == 0 {
		return false, 0
	} else if event.EventType == PRIVATE && details.TotalAvailable > 0  && details.Available > 0 {
		return true, int32(details.Available)
	}
	return false, 0
}

func (db *PlayDb) CreateNotification(n_type int, obj interface{}) {

	notification := new(play.Notification)

	switch n_type {

	case WELCOME_MSG:
		// Welcome message. Type should be user.
		currUser := obj.(*play.UserInfo)

		notification.NotificationTitle = "Welcome" + currUser.Name + " !"
		notification.NotificationMessage = "Hi " + currUser.Name + ", \n\n" + "\tWelcome to Play.io. Now you can " +
			"join sporting events around the city! You can browse the events happening around you " +
			"create your own events and invite friends." +
			"\nStay Healthy!\nPlay.io Team."

		notification.SenderName = "Play.io Team"
		notification.SenderUrl = "admin_user"
		notification.SentAt = uint32(time.Now().Unix())
		notification.EventName = ""
		notification.EventUrl = ""
		notification.UserUuid = currUser.UserUuid
		notification.NotificationType = WELCOME_MSG
		db.SendNotification(notification, currUser.GcmRegId)
		break

	case JOIN_REQUEST:
		join_req := obj.(*play.EventJoinRequest)

		receiver, st := db.GetUserByIdDB(&play.ReqUUID{Uuid:join_req.OrgUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, join_req)
		}

		sender, st := db.GetUserByIdDB(&play.ReqUUID{Uuid:join_req.UserUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, join_req)
		}

		event, st := db.GetEventByIdDB(&play.ReqUUID{Uuid:join_req.EventUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, join_req)
		}

		details, st := db.GetEventAvailabilityInfoDB(&play.ReqUUID{Uuid:event.EventUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, join_req)
		}

		notification.NotificationTitle = sender.Name + " wants to join your event."
		notification.NotificationType = JOIN_REQUEST
		notification.EventName = event.EventName
		notification.EventUrl = "play_event://"+event.EventUuid
		notification.UserUuid = receiver.UserUuid
		notification.SenderName = sender.Name
		notification.SenderUrl = "play_user://"+sender.UserUuid
		notification.SentAt = uint32(time.Now().Unix())
		if details.TotalAvailable == 0 {
			notification.NotificationMessage =
				BufferStrings("Do you want to accept ",
					sender.Name,
					"'s request to join your event ",
					event.EventName,
					getTimeStampString(time.Unix(int64(event.EventStart), 0)),
					"?")
		} else if details.Available > 0 {
			notification.NotificationMessage =
				BufferStrings("Do you want to accept ",
					sender.Name,
					"'s request to join your event ",
					event.EventName,
					getTimeStampString(time.Unix(int64(event.EventStart), 0)),
					"?\nThere are ",
					strconv.Itoa(int(details.Available)),
					" spots available.")
		} else {
			notification.NotificationMessage =
				BufferStrings("You event ",
					event.EventName,
					" has reached full capacity.",
					sender.Name,
					" wants to join.\nYou can consider taking on more people to cover any last minute cancellations.")
		}
		db.SendNotification(notification, receiver.GcmRegId)
		break
	case REQUEST_ACCEPTED:
		join_req := obj.(*play.EventJoinRequest)

		receiver, st := db.GetUserByIdDB(&play.ReqUUID{Uuid:join_req.UserUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, join_req)
		}

		sender, st := db.GetUserByIdDB(&play.ReqUUID{Uuid:join_req.OrgUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, join_req)
		}

		event, st := db.GetEventByIdDB(&play.ReqUUID{Uuid:join_req.EventUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, join_req)
		}

		details, st := db.GetEventAvailabilityInfoDB(&play.ReqUUID{Uuid:event.EventUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, join_req)
		}

		if event.EventType == PUBLIC {
			notification.NotificationTitle = "You have joined " + event.EventName
			notification.SenderName = ""
			notification.SenderUrl = ""
		} else {
			notification.NotificationTitle = sender.Name + " has accepted your request."
			notification.SenderName = sender.Name
			notification.SenderUrl = "play_user://"+sender.UserUuid
		}

		notification.NotificationType = REQUEST_ACCEPTED
		notification.EventName = event.EventName
		notification.EventUrl = "play_event://"+event.EventUuid
		notification.UserUuid = receiver.UserUuid
		notification.SentAt = uint32(time.Now().Unix())
		notification.NotificationMessage =
			BufferStrings("You are now registered for the event!\nNow you can ",
				"add this event to your calendar or you can also see the event details in the ",
				"'My Events' tab.")

		if yes, count := db.canInviteMorePeople(event, details); yes {
			if count > 0 {
				notification.NotificationMessage =
					BufferStrings(notification.NotificationMessage,
						"\nThere are ",
						strconv.Itoa(int(details.Available)),
						" spots left. You can invite more people to this event!")
			} else {
				notification.NotificationMessage =
					BufferStrings(notification.NotificationMessage,
						"\nYou can also invite more people to join this event.")
			}
		}
		db.SendNotification(notification, receiver.GcmRegId)
		break
	case INVITE_ACCEPTED:
		invite := obj.(*play.EventInvite)

		receiver, st := db.GetUserByIdDB(&play.ReqUUID{Uuid:invite.AccUserUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, invite)
		}

		sender, st := db.GetUserByIdDB(&play.ReqUUID{Uuid:invite.UserUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, invite)
		}

		event, st := db.GetEventByIdDB(&play.ReqUUID{Uuid:invite.EventUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, invite)
		}

		details, st := db.GetEventAvailabilityInfoDB(&play.ReqUUID{Uuid:event.EventUuid})
		if st != SUCCESS {
			_ = db.GetAppError(st, invite)
		}

		notification.NotificationTitle = "You have accepted " + sender.Name + "'s invite."
		notification.SenderName = sender.Name
		notification.SenderUrl = "play_user://"+sender.UserUuid
		notification.EventName = event.EventName
		notification.EventUrl = "play_event://"+event.EventUuid
		notification.UserUuid = receiver.UserUuid
		notification.SentAt = uint32(time.Now().Unix())
		notification.NotificationMessage =
			BufferStrings("You are now registered for ",
				event.EventName,
				getTimeStampString(time.Unix(int64(event.EventStart), 0)),
				"!\nNow you can add this event to your calendar or you can also see the event details in the ",
				"'My Events' tab.")

		if yes, count := db.canInviteMorePeople(event, details); yes {
			if count > 0 {
				notification.NotificationMessage =
					BufferStrings(notification.NotificationMessage,
						"\nThere are ",
						strconv.Itoa(int(details.Available)),
						" spots left. You can invite more people to this event!")
			} else {
				notification.NotificationMessage +=
					BufferStrings(notification.NotificationMessage,
						"\nYou can also invite more people to join this event.")

			}
		}
		db.SendNotification(notification, receiver.GcmRegId)
		break
	default:
		break
	}

}

func (db *PlayDb) SendNotification(notification *play.Notification, gcm_id string) {

	data := structs.Map(notification)
	msg := gcm.NewMessage(data, gcm_id)

	// Create a Sender to send the message.
	sender := &gcm.Sender{ApiKey: "AIzaSyC7OTbHRvUK_1koX-zWUAcKP8XkLTLEGS0"}

	// Send the message and receive the response after at most two retries.
	response, err := sender.Send(msg, 2)
	if err != nil {
		log.Print("Failed to send msg")
	}
	if response.Success == 1 {
		log.Print("Notification sent")
	}
}
