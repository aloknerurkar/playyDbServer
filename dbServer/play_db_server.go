package main

import (
	"flag"
	"net"
	"fmt"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc"
	play "playyDbServer"
	pdb "playyDbServer/dbHandler"
	"golang.org/x/net/context"
)

var (
	tls        = flag.Bool("tls", false, "Connection uses TLS if true, else plain TCP")
	certFile   = flag.String("cert_file", "testdata/server1.pem", "The TLS cert file")
	keyFile    = flag.String("key_file", "testdata/server1.key", "The TLS key file")
	port       = flag.Int("port", 10000, "The server port")
)

type playDbServer struct  {
	server *play.PlayDbServiceServer
	dbServer *pdb.PlayDb
}

func newServer() *playDbServer {
	s := new(playDbServer)
	s.server = new(play.PlayDbServiceServer)
	s.dbServer = pdb.NewPlayDb()
	return s
}

// User
func (s *playDbServer) CreateUser(c context.Context, user_req *play.UserInfo) (*play.UserInfo, error) {

	if user_req.Name == "" ||
		user_req.Mobile == "" ||
		user_req.GcmRegId == "" ||
		(user_req.Email == "" && user_req.Mobile == "") {

		return user_req, s.dbServer.GetAppError(pdb.INCORRECT_REQ, user_req)
	}

	user_resp, st := s.dbServer.CreateUserDB(user_req)

	return user_resp, s.dbServer.GetAppError(st, user_req)
}

func (s *playDbServer) GetUserInfoById(c context.Context, uuid *play.ReqUUID) (*play.UserInfo, error) {

	if uuid.Uuid == "" {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, uuid)
	}

	org, st := s.dbServer.GetUserByIdDB(uuid)

	return org, s.dbServer.GetAppError(st, uuid)
}

func (s *playDbServer) GetUserByInfo(c context.Context, user *play.UserInfo) (*play.UserInfo, error) {

	if len(user.Email) == 0 && len(user.Mobile) == 0 {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, user)
	}

	user_out, st := s.dbServer.GetUserByInfoDB(user)

	return user_out, s.dbServer.GetAppError(st, user)
}

// Organizer
func (s *playDbServer) CreateOrganizer(c context.Context, org *play.Organizer) (*play.Organizer, error) {

	if org.OrgName == "" || (org.OrgEmail == "" && org.OrgPh == "") {
		return org, s.dbServer.GetAppError(pdb.INCORRECT_REQ, org)
	}

	org_resp, st := s.dbServer.CreateOrganizerDB(org)

	return org_resp, s.dbServer.GetAppError(st, org)
}

func (s *playDbServer) GetOrganizerById(c context.Context, uuid *play.ReqUUID) (*play.Organizer, error) {

	if uuid.Uuid == "" {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, uuid)
	}

	org, st := s.dbServer.GetOrganizerByIdDB(uuid)

	return org, s.dbServer.GetAppError(st, uuid)
}

// Event
func (s *playDbServer) CreateEvent(c context.Context, req_event *play.Event) (*play.Event, error) {

	if len(req_event.EventName) == 0 || req_event.EventStart == 0 ||
		req_event.EventEnd == 0 || len(req_event.VenueUuid) == 0 ||
		len(req_event.OrganizerUuid) == 0 {

		return req_event, s.dbServer.GetAppError(pdb.INCORRECT_REQ, req_event)
	}

	ev, st := s.dbServer.CreateEventDB(req_event)

	return ev, s.dbServer.GetAppError(st, req_event)
}

func (s *playDbServer) GetEventsByVenue(c context.Context, uuid *play.ReqUUID) (*play.Events, error) {

	if uuid.Uuid == "" {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, uuid)
	}

	events, st := s.dbServer.GetEventsByVenueDB(uuid)

	// Can be '0'
	if st == pdb.SQL_NO_ROWS {
		st = pdb.SUCCESS
	}

	return events, s.dbServer.GetAppError(st, uuid)
}

func (s *playDbServer) GetEventById(c context.Context, uuid *play.ReqUUID) (*play.Event, error) {

	if uuid.Uuid == "" {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, uuid)
	}

	event, st := s.dbServer.GetEventByIdDB(uuid)

	return event, s.dbServer.GetAppError(st, uuid)
}

func (s *playDbServer) GetEventAvailabilityInfo(c context.Context, uuid *play.ReqUUID) (*play.EventAvailabilityInfo, error) {

	if uuid.Uuid == "" {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, uuid)
	}

	ea_info, st := s.dbServer.GetEventAvailabilityInfoDB(uuid)

	return ea_info, s.dbServer.GetAppError(st, uuid)
}

func (s *playDbServer) GetEventPlayersInfo(c context.Context, uuid *play.ReqUUID) (*play.EventPlayersInfo, error) {

	if uuid.Uuid == "" {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, uuid)
	}

	ep_info, st := s.dbServer.GetEventPlayersInfoDB(uuid)

	return ep_info, s.dbServer.GetAppError(st, uuid)
}

func (s *playDbServer) GetEventsParticipationByUserId(c context.Context, uuid *play.ReqUUID) (*play.EventParticipations, error) {

	if uuid.Uuid == "" {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, uuid)
	}

	participations, st := s.dbServer.GetEventsParticipationDB(uuid)

	return participations, s.dbServer.GetAppError(st, uuid)
}

//Venue
func (s *playDbServer) CreateVenue(c context.Context, req_venue *play.Venue) (*play.Venue, error) {

	if len(req_venue.VenueName) == 0 || len(req_venue.Address_1) == 0 ||
		len(req_venue.City) == 0 || len(req_venue.Latitude) == 0 ||
		len(req_venue.Longitude) == 0 || len(req_venue.Zip) == 0 {

		return req_venue, s.dbServer.GetAppError(pdb.INCORRECT_REQ, req_venue)
	}

	vn, st := s.dbServer.CreateVenueDB(req_venue)

	return vn, s.dbServer.GetAppError(st, req_venue)
}

func (s *playDbServer) GetVenuesByLocation(c context.Context, loc *play.LocationInfo) (*play.Venues, error) {

	if len(loc.Latitude) == 0 || len(loc.Longitude) == 0 {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, loc)
	}

	venues, st := s.dbServer.GetVenuesByLocationDB(loc)

	// Can be '0'
	if st == pdb.SQL_NO_ROWS {
		st = pdb.SUCCESS
	}

	return venues, s.dbServer.GetAppError(st, loc)
}

func (s *playDbServer) GetVenueById(c context.Context, uuid *play.ReqUUID) (*play.Venue, error) {

	if uuid.Uuid == "" {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, uuid)
	}

	venue, st := s.dbServer.GetVenueByIdDB(uuid)

	return venue, s.dbServer.GetAppError(st, uuid)
}

func (s *playDbServer) CreateEventJoinReq(c context.Context, join_req *play.EventJoinRequest) (*play.EventJoinRequest, error) {

	if len(join_req.EventUuid) == 0 || len(join_req.UserUuid) == 0 ||
		len(join_req.OrgUuid) == 0 {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, join_req)
	}

	resp, st := s.dbServer.CreateEventJoinReqDB(join_req)

	return resp, s.dbServer.GetAppError(st, join_req)
}

func (s *playDbServer) CreateEventInvite(c context.Context, invite *play.EventInvite) (*play.EventInvite, error) {

	if len(invite.EventUuid) == 0 || len(invite.UserUuid) == 0 {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, invite)
	}

	resp, st := s.dbServer.CreateEventInviteReqDB(invite)

	return resp, s.dbServer.GetAppError(st, invite)
}

func (s *playDbServer) CreateEventParticipant(c context.Context, player *play.EventParticipant) (*play.EventParticipant, error) {

	if len(player.EventUuid) == 0 || len(player.UserUuid) == 0 {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, player)
	}

	resp, st := s.dbServer.CreateEventParticipantDB(player)

	return resp, s.dbServer.GetAppError(st, player)
}

func (s *playDbServer) AcceptEventJoinReq(c context.Context, join_req *play.EventJoinRequest) (*play.EventJoinRequest, error) {

	if len(join_req.EventUuid) == 0 || len(join_req.UserUuid) == 0 ||
		len(join_req.OrgUuid) == 0 {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, join_req)
	}

	resp, st := s.dbServer.AcceptEventJoinReqDB(join_req)

	return resp, s.dbServer.GetAppError(st, join_req)
}

func (s *playDbServer) AcceptEventInvite(c context.Context, invite *play.EventInvite) (*play.EventInvite, error) {

	if len(invite.EventUuid) == 0 || len(invite.UserUuid) == 0 {
		return nil, s.dbServer.GetAppError(pdb.INCORRECT_REQ, invite)
	}

	resp, st := s.dbServer.AcceptEventInviteDB(invite)

	return resp, s.dbServer.GetAppError(st, invite)
}

func (s *playDbServer) GetHeartbeat(c context.Context, emp *play.EmptyMessage) (*play.EmptyMessage, error)  {
	//HeartBeat query
	return &play.EmptyMessage{}, nil
}


func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		grpclog.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption
	if *tls {
		creds, err := credentials.NewServerTLSFromFile(*certFile, *keyFile)
		if err != nil {
			grpclog.Fatalf("Failed to generate credentials %v", err)
		}
		opts = []grpc.ServerOption{grpc.Creds(creds)}
	}
	grpcServer := grpc.NewServer(opts...)
	play.RegisterPlayDbServiceServer(grpcServer, newServer())
	grpcServer.Serve(lis)
}