CREATE TABLE Users (
	user_uuid varchar(100) NOT NULL,
	name varchar(255) NOT NULL,
	email varchar(255),
	mobile varchar(50),
	gcm_reg_id varchar(255) NOT NULL,
	PRIMARY KEY(user_uuid)
);

CREATE TABLE Organizers (
    org_uuid varchar(100) NOT NULL,
    org_name varchar(255) NOT NULL,
    org_url varchar(255) DEFAULT '',
    org_email varchar(255),
    org_ph varchar(50),
    org_contact_name varchar(255) DEFAULT '',
    user_uuid varchar(100) NOT NULL,
    PRIMARY KEY(org_uuid),
    FOREIGN KEY(user_uuid) REFERENCES Users(user_uuid)
);

CREATE TABLE Venues (
	venue_uuid varchar(100) NOT NULL,
	venue_name varchar(255) NOT NULL,
	venue_url varchar(255),
	domain_name varchar(255),
    domain_id varchar(255),
	address_1 varchar(255) NOT NULL,
	address_2 varchar(255) DEFAULT '',
	city varchar(255) NOT NULL,
	state varchar(255),
	zip varchar(50),
	latitude varchar(100) NOT NULL,
	longitude varchar(100) NOT NULL,
	event_cat_venue int,
	PRIMARY KEY(plan_uuid)
);

CREATE TABLE Events (
	event_uuid varchar(100) NOT NULL,
	event_name varchar(100) NOT NULL,
	event_url varchar(255),
	domain_name varchar(255),
    domain_id varchar(255),
	event_start int NOT NULL,
	event_end int,
	venue_uuid varchar(100) NOT NULL,
	organizer_uuid varchar(100) NOT NULL,
	description text,
	event_cat int,
	event_type int,
	PRIMARY KEY(event_uuid),
	FOREIGN KEY(venue_uuid) REFERENCES Venues(venue_uuid),
	FOREIGN KEY(org_uuid) REFERENCES Organizers(org_uuid)
);

CREATE TABLE EventAvailabilityInfo (
    event_uuid varchar(100) NOT NULL,
    sales_start int DEFAULT 0,
    sales_end int DEFAULT 0,
    cost int DEFAULT 0,
    total_available int DEFAULT 0,
    sold int DEFAULT 0,
    available int DEFAULT 100, #Default high value.
    PRIMARY KEY(event_uuid),
    FOREIGN KEY(event_uuid) REFERENCES Events(event_uuid)
);

CREATE TABLE EventPlayersInfo (
    event_uuid varchar(100) NOT NULL,
    min_players int DEFAULT 0,
    max_players int DEFAULT 0,
    min_age int DEFAULT 0,
    max_age int DEFAULT 0,
    PRIMARY KEY(event_uuid),
    FOREIGN KEY(event_uuid) REFERENCES Events(event_uuid)
);

CREATE TABLE EventParticipants (
    event_uuid varchar(100) NOT NULL,
    org_uuid varchar(100) NOT NULL,
    user_uuid varchar(100) NOT NULL,
    joined_at int NOT NULL,
    PRIMARY KEY(event_uuid, user_uuid),
    FOREIGN KEY(event_uuid) REFERENCES Events(event_uuid),
    FOREIGN KEY(org_uuid) REFERENCES Organizers(org_uuid)
);

CREATE TABLE EventJoinRequests (
    event_uuid varchar(100) NOT NULL,
    org_uuid varchar(100) NOT NULL,
    user_uuid varchar(100) NOT NULL,
    created_at int NOT NULL,
    sent_at int DEFAULT 0,
    accepted_at int DEFAULT 0,
    PRIMARY KEY(event_uuid, user_uuid),
    FOREIGN KEY(event_uuid) REFERENCES Events(event_uuid),
    FOREIGN KEY(org_uuid) REFERENCES Organizers(org_uuid),
    FOREIGN KEY(user_uuid) REFERENCES Users(user_uuid)
);

CREATE TABLE EventInvites (
    event_uuid varchar(100) NOT NULL,
    user_uuid varchar(100) NOT NULL,
    invite_code varchar(100) NOT NULL,
    created_at int NOT NULL,
    sent_at int DEFAULT 0,
    accepted_at int DEFAULT 0,
    acc_user_uuid varchar(100) DEFAULT '',
    PRIMARY KEY(event_uuid, user_uuid, invite_code),
    FOREIGN KEY(event_uuid) REFERENCES Events(event_uuid),
    FOREIGN KEY(user_uuid) REFERENCES Users(user_uuid)
);

CREATE TABLE Notifications (
	notification_id int AUTO_INCREMENT,
	notification_type int,
	notification_title varchar(255) NOT NULL,
	user_uuid varchar(100) NOT NULL,
	sender_name varchar(255),
	sender_url varchar(255),
	event_name varchar(255) DEFAULT '',
	event_url varchar(255) DEFAULT '',
	sent_at int NOT NULL,
	notification_message text NOT NULL,
	PRIMARY KEY(notification_id),
	FOREIGN KEY(user_uuid) REFERENCES Users(user_uuid)
);